Một thư viện cần quản lý các tài liệu bao gồm Sách, Tạp chí, Báo. 
Mỗi tài liệu gồm có các thuộc tính sau: 
Mã tài liệu(Mã tài liệu là duy nhất), Tên nhà xuất bản, số bản phát hành.

Các loại sách cần quản lý thêm các thuộc tính: tên tác giả, số trang.

Các tạp chí cần quản lý thêm: Số phát hành, tháng phát hành.

Các báo cần quản lý thêm: Ngày phát hành.

Yêu cầu 1: Xây dựng các lớp để quản lý tài liệu cho thư viện một cách hiệu quả.

Yêu cầu 2: Xây dựng lớp QuanLySach có các chức năng sau

Thêm mới tài liêu: Sách, tạp chí, báo.
Xoá tài liệu theo mã tài liệu.
Hiện thị thông tin về tài liệu.
Tìm kiếm tài liệu theo loại: Sách, tạp chí, báo.
Thoát khỏi chương trình.

Gợi ý
giả dụ ta có 1 cái table document bao gồm thông tin type, id, desc, name
khi thêm mới book thì ta chỉ cần đưa thông cơ bản của book vào trong table document
tương tự như newspaper, journal

Hướng 1 tao tạo ra 1 lớp quản lý docmument
bao gồm các dịch vụ thêm mới , sửa, xoá, view