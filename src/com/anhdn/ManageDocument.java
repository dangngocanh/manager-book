package com.anhdn;

import java.util.ArrayList;
import java.util.List;

public class ManageDocument {
    // lop nay giong nhu 1 table trong database
    private List<Document> documents;// cai nay se la 1 table luu thong tin doc

    public ManageDocument() {
        this.documents = new ArrayList<>();
    }

    public boolean addDocument(Document document){
        return this.documents.add(document);
    }

    public List<Document> getDocuments(){
        return this.documents;
    }

}
