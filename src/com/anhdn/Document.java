package com.anhdn;

import java.util.*;

public class Document {
   // copy de bai vao day
    //Mã tài liệu(Mã tài liệu là duy nhất), Tên nhà xuất bản, số bản phát hành.
    // gio tao property nha xuat ban transate, so phat hành issue number
    // sai mac phim nhu cc
    // gio tao them cac the hien cua document
    private String documentCode;
    private String publishingCompany;
    private Long releaseNumber;

    // mang may nat the
    Document(String documentCode, String publishingCompany, Long releaseNumber){
        this.documentCode = documentCode;
        this.publishingCompany = publishingCompany;
        this.releaseNumber = releaseNumber;
    }
    //sao ko dung generate ghi cho m hieu


    @Override
    public String toString() {
        return "Document{" +
                "documentCode='" + documentCode + '\'' +
                ", publishingCompany='" + publishingCompany + '\'' +
                ", releaseNumber=" + releaseNumber +
                '}';
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    public String getPublishingCompany() {
        return publishingCompany;
    }

    public void setPublishingCompany(String publishingCompany) {
        this.publishingCompany = publishingCompany;
    }

    public Long getReleaseNumber() {
        return releaseNumber;
    }

    public void setReleaseNumber(Long releaseNumber) {
        this.releaseNumber = releaseNumber;
    }
}
