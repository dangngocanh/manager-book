package com.anhdn;

import java.util.ArrayList;

public class Book extends Document{

    // tên tác giả, số trang lam tuong tu nhu journal, new???? lam xong r
    private String author;
    private int numberPage;

    Book(String documentCode, String publishingCompany, Long releaseNumber, String author, int numberPage) {
        super(documentCode, publishingCompany, releaseNumber);
        this.author = author;
        this.numberPage = numberPage;
    }


    public void findBook(){

    }

    public void removeItemBook(){

    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberPage() {
        return numberPage;
    }

    public void setNumberPage(int numberPage) {
        this.numberPage = numberPage;
    }
}
