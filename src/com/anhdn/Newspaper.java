package com.anhdn;

import java.util.Date;

public class Newspaper extends Document{
    // Báo
    // thêm Ngày phát hành
    private Date dayPublish;

    public Newspaper(String documentCode, String publishingCompany, Long releaseNumber, Date dayPublish) {
        super(documentCode, publishingCompany, releaseNumber);
        this.dayPublish = dayPublish;
    }

    public Date getDayPublish() {
        return dayPublish;
    }

    public void setDayPublish(Date dayPublish) {
        this.dayPublish = dayPublish;
    }
}
