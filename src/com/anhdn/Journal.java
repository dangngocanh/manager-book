package com.anhdn;

public class Journal extends Document {
    // tạp chí
    // thêm số phát hành, tháng phát hành
    private int numberPublish;
    private int monthPublish;

    public Journal(String documentCode, String publishingCompany, Long releaseNumber, int numberPublish, int monthPublish) {
        super(documentCode, publishingCompany, releaseNumber);
        this.numberPublish = numberPublish;
        this.monthPublish = monthPublish;
    }

    public int getNumberPublish() {
        return numberPublish;
    }

    public void setNumberPublish(int numberPublish) {
        this.numberPublish = numberPublish;
    }

    public int getMonthPublish() {
        return monthPublish;
    }

    public void setMonthPublish(int monthPublish) {
        this.monthPublish = monthPublish;
    }
}
